namespace ElevenNote.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Note", "Content", c => c.String(nullable: false));
            DropColumn("dbo.Note", "Context");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Note", "Context", c => c.String(nullable: false));
            DropColumn("dbo.Note", "Content");
        }
    }
}
