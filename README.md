# Eleven Note
Eleven Note is an n-tier application that incorporates an MVC and API presentation layer. It is written in C#, .NET & Entity Framework. This application also uses MS-SQL for the database.  

The user has the ability to create, read, update or delete any notes after creating an account. 

## Solution Structure

Data: This is where we setup our database using the Entity Framework.

Services: This is the messenger that helps the data and presentation tiers communicate. An example of this would be authentication and authorization with account registration.

Models: This view model represents the data that we want to show on the page. 

WebAPI : This is the API, or also known as the presentation tier of the application.



